import React, { useEffect, useContext, useState } from "react";
import config from "visual-config-exposer";
import Styled from "styled-components";

import { GameContext } from "../../context/gameContext";
import { ScoreContext } from "../../context/scoreContext";
import LeaderBoard from "../LeaderBoard/LeaderBoard";
import "./postQuiz.css";

let post = true;

const PostQuiz = () => {
  const [showScores, setShowScores] = useState(false);
  const [score, setScore] = useState(null);

  const scoreContext = useContext(ScoreContext);
  const gameContext = useContext(GameContext);

  const LeaderBoardButton = Styled.button`
  background-color: ${config.preQuizScreen.leaderBoardBtnColor};
  color: ${config.preQuizScreen.leaderBoardBtnTextColor};
  `;

  const playAgain = () => {
    gameContext.setMainScreen();
    scoreContext.resetScore(0);
    post = true;
  };

  const submitHandler = () => {
    const username = document.getElementById("username").value;
    const email = document.getElementById("email").value;
    const score = scoreContext.score;
    const scoreData = {
      display_name: username,
      email: email,
      score: score,
    };
    setScore(scoreData);
    setShowScores(true);
  };

  const changeLeaderBoardHandler = () => {
    setShowScores(!showScores);
    post = false;
  };

  return (
    <div>
      {!showScores ? (
        <article className="pre__card">
          <div>
            <h1
              className="post__title"
              style={{ color: config.postQuizScreen.scoreTextColor }}
            >
              {config.postQuizScreen.scoreText} : {scoreContext.score}
            </h1>
          </div>
          {!score ? (
            <div className="score_cta">
              <div className="submit">
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    width: "220px",
                  }}
                >
                  <label htmlFor="username" className="submit_label">
                    Username
                  </label>
                  <input
                    type="text"
                    className="submit_input"
                    name="username"
                    id="username"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    width: "220px",
                  }}
                >
                  <label htmlFor="email" className="submit_label">
                    Email
                  </label>
                  <input
                    type="email"
                    className="submit_input"
                    name="email"
                    id="email"
                  />
                </div>
                <button
                  style={{ width: "140px" }}
                  className="cta-button"
                  onClick={submitHandler}
                >
                  Submit Score
                </button>
              </div>
              {config.postQuizScreen.showCta && (
                <div className="cta">
                  <h4 className="cta-text">{config.postQuizScreen.ctaText}</h4>
                  <a
                    href={config.postQuizScreen.ctaUrl}
                    target="_blank"
                    rel="noopener noreferer"
                    className="cta-link"
                  >
                    <button className="cta-button">
                      {config.postQuizScreen.ctaBtnText}
                    </button>
                  </a>
                </div>
              )}
            </div>
          ) : (
            <div>
              {config.postQuizScreen.showCta && (
                <div className="cta">
                  <h4 className="cta-text">{config.postQuizScreen.ctaText}</h4>
                  <a
                    href={config.postQuizScreen.ctaUrl}
                    target="_blank"
                    rel="noopener noreferer"
                    className="cta-link"
                  >
                    <button className="cta-button">
                      {config.postQuizScreen.ctaBtnText}
                    </button>
                  </a>
                </div>
              )}
              <div className="pre__btn-container">
                <button
                  className="button"
                  onClick={changeLeaderBoardHandler}
                  style={{
                    color: config.postQuizScreen.againButtonTextColor,
                    backgroundColor: config.postQuizScreen.againButtonColor,
                  }}
                >
                  LeaderBoard
                </button>
              </div>
            </div>
          )}
          <div className="pre__btn-container">
            <button
              className="button"
              onClick={playAgain}
              style={{
                color: config.postQuizScreen.againButtonTextColor,
                backgroundColor: config.postQuizScreen.againButtonColor,
              }}
            >
              {config.postQuizScreen.againButtonText}
            </button>
          </div>
        </article>
      ) : (
        <article className="pre__card">
          <LeaderBoard post={post} score={score} />
          <LeaderBoardButton
            className="button"
            onClick={changeLeaderBoardHandler}
          >
            back
          </LeaderBoardButton>
        </article>
      )}
    </div>
  );
};

export default PostQuiz;
