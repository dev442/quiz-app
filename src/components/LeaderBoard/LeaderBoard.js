import React, { useEffect, useState } from "react";
import Database from "database-api";

import "./LeaderBoard.css";

let database = new Database();

const LeaderBoard = (props) => {
  let Data;
  const [data, setData] = useState([]);

  useEffect(() => {
    postLeaderBoardHandler();
    return () => {
      setData([]);
    };
  });

  const postLeaderBoardHandler = () => {
    console.log(props);
    if (props.post) {
      database.postScoreData(props.score).then(() => {
        getLeaderBoardHandler();
      });
    } else {
      getLeaderBoardHandler();
    }
  };

  const getLeaderBoardHandler = () => {
    try {
      database.getLeaderBoard().then((data) => {
        let sortedData = data.sort(
          (a, b) => parseInt(a.score) < parseInt(b.score)
        );
        setData(sortedData);
      });
    } catch (err) {
      throw new Error(err);
    }
  };

  if (data) {
    Data = data.map((element, index) => (
      <li className="row" key={index}>
        <div>{index + 1}</div>
        <div>{element.display_name}</div>
        <div>{element.score}</div>
      </li>
    ));
  }

  return (
    <div className="ldbrd__container">
      <h2 className="ldbrd__heading">LeaderBoard</h2>
      <ul className="ldbrd__scores">
        <li
          className="row"
          style={{ fontWeight: "bolder", marginBottom: "10px" }}
        >
          <div>No.</div>
          <div>Name</div>
          <div>Score</div>
        </li>
        {Data}
      </ul>
    </div>
  );
};

export default LeaderBoard;
